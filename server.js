const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const formData = require('express-form-data')
const cors = require('cors');

app.use(cors());
app.use(bodyParser.json());
app.use(formData.parse())

app.post('/', function (req, res) {
    res.json({
        docUrl: 'https://cdn2.hubspot.net/hubfs/3893470/Imported%20blog%20content/He%E2%80%99s-So-Fluffy-Unicorn-Card-Tag.pdf'
    })
})

app.get('/', function (req, res) {
    res.send('you have reacked fake api')
})

const port = parseInt(process.env.PORT, 10) || 4000;
app.listen(port, function () {
    console.log("Server is running on Port: " + port);
});